var path = require('path');
var fs = require('fs-extra');

var Viz = require('viz.js');
var svg2png = require('svg2png');
var escape = require('escape-html');

var dormouse = require('dormouse');
var inline = require('./inline.js');

var options = {
    glob : 'src/**/*.js',
    keyword : 'markdown',
    output : 'README.md',

    processor : async function (node, source) {
        var prop = node.properties;
        var directory = path.dirname(source.id);
        var text = '';

        if (prop.source_compiled) {

            try {
                node.text = eval(`(function () { ${node.text} }());`);

                if (typeof node.text === "object") {
                    try {
                        node.text = JSON.stringify(node.text, null, 3);
                    } catch(err) {
                        node.text = node.text.toString();
                    }
                }

            } catch (err) {
                console.log('attempted to compile non-compilable Javascript code:');
                console.log(node.text);
                throw err;
            }
        }

        if (prop.source_compiled || prop.source) {

            /** Allow specifying the base language of the output */
            let type = (prop.source || prop.source_compiled);
            if (typeof type !== 'string') { type = 'js'; }

            text = [
                '```' + type,
                node.text,
                '```'
            ].join('\n');
        }

        if (prop.dormouse) {
            let graph = await dormouse(node.text);

            text = [
                text,
                '```js',
                JSON.stringify(graph, null, 3),
                '```'
            ].join('\n');
        }

        if (prop.dot) {

            let svg = Viz(node.text, {
                format : 'svg'
            });

            let output = (typeof prop.dot === 'string') ? prop.dot : node.id;
            output = (path.extName === 'png') ? output : output + '.png'; /* postfixing is optional */

            /* Workaround for https://gitlab.com/gitlab-org/gitlab-ce/issues/17987 */
            await (svg2png(svg)
                   .then(async function (buffer) {
                       await fs.ensureDir(path.resolve(directory, '.images'));
                       return fs.writeFile(path.resolve(directory, '.images', output), buffer);
                   }));

            /*
             * Gitlab (and Markdown in general, probably) has problems with multiline alt text.
             * It can be handled, but it needs to be separated by (at most) 1 line.
             */
            let alt = node.text
                    .split(/[\n\r]/)
                    .filter(function (line) {
                        return line.trim() !== '';
                    })
                    .join('\n');

            text = [
                text,
                `<img src="./.images/${output}"  alt="${escape(alt)}"/>`
            ].join('\n');
        }

        return text || node.text;
    }
};

inline(options)
    .then(async function () {
        var source = path.resolve('src/tests/base/');
        var destination = path.resolve('');

        return Promise.all([
            fs.copy(source + '/README.md', destination + '/README.md'),
            fs.copy(source + '/.images', destination + '/.images')
        ]);
    })
    .catch(function (err) {
        console.log(err);
        process.exitCode = 1;
    });
