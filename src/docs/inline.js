var promisify = require('util').promisify;
var path = require('path');
var fs = require('fs');
var writeFile = promisify(fs.writeFile);

var dormouse = require('dormouse');


async function reduce(array, method, result) {
    for (let i = 0; i < array.length; i++) {
        result = await method(result, array[i]);
    }

    return result;
}

async function generate(options) {

    var glob = options.glob;
    var keyword = options.keyword;
    var output = options.output;
    var process = options.processor || function (node) { return node.text; };

    var graph = await dormouse(glob, {
        type : 'glob',
        source : 'source'
    });

    var nodes = graph[keyword].links.sort();

    var directories = await reduce(nodes, async function (directories, id) {
        var node = graph[id];
        var source = graph[node.source];
        var directory = path.dirname(source.id);

        var text = await process(node, source);

        directories[directory] = directories[directory] || '';
        directories[directory] = [
            directories[directory],
            text
        ].join('\n\n');

        return directories;
    }, {});

    var writes = Object.keys(directories).map(function (directory) {

        return writeFile(
            path.resolve(directory, output),
            directories[directory],
            'utf8'
        );
    });

    return Promise.all(writes);
}

module.exports = generate;
