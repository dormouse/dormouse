var dormouse = require('dormouse');
var promisify = require('util').promisify;
var path = require('path');
var fs = require('fs');


(async function generate () {

    var files = 'src/**/*.js';

    var graph = await dormouse(files, {
        type : 'glob',

        includeSource : true,
        includeLocation : true,

        sourceIndex : 'name',
        source : 'souce'
    });

    var todos = graph.todo.links.reduce(function (result, id) {
        var todo = graph[id];
        var source = graph[todo.source];

        result[source.id] = result[source.id] || [];
        result[source.id].push(todo);

        return result;
    }, {});

    var output = Object.keys(todos).reduce(function (output, filename) {

        output.push(`* ${filename}`);

        todos[filename].forEach(function (node) {
            var lines = node.text.split(/[\n\r]/);

            var location =
                    (node.line != null) ? node.line :
                    /* @todo (node.location != null) ? utils.text.line(graph[filename].source, node.location) : */
                    lines[0].slice(0, lines[0].indexOf('[') - 1);

            output.push(`** TODO: ${lines[0]}`);
            output.push(`[[file:${filename}::${location}][Context]]`);

            output.push('');
            output = output.concat(lines.slice(1));
            output.push('');

        });

        return output;
    }, []);

    await promisify(fs.writeFile)(
        path.resolve('TODO.org'),
        output.join('\n'),
        'utf8'
    );

}());
