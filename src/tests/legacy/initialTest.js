var suite = require('helpers/suite');
var assert = require('helpers/assert');
var simplify = require('helpers/simplify');

var dormouse = require('dormouse');

/*`` @test */
suite.test('single-line comments', async function () {
    var source = (function () {
        /** Line 1 */
    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : { text : 'Line 1' }
    });
});
/*``*/

/*`` @test */
suite.test('mulit-line comments', async function () {
    var source = (function () {

        /**
         * Line 1
         * Line 2
         * Line 3
         */

    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : {
            text : ['Line 1',
                    'Line 2',
                    'Line 3'
                   ].join('\n')
        }
    });
});
/*``*/

/*`` @test */
suite.test('spacing within comments', async function () {
    var source = (function () {

        /**
         * Line 1
         *
         * Line 2
         */
    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : {
            text : ['Line 1',
                    '',
                    'Line 2'
                   ].join('\n')
        }
    });
});
/*``*/

/*`` @test */
suite.test('multiple comments', async function () {

    var source = (function () {
        /** Comment */
        /** Comment 2 */
    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : {
            links : [],
            text : 'Comment'
        },
        _1 : {
            links : [],
            text : 'Comment 2'
        }
    });
});
/*``*/

/**
 * Any node can be given an arbitrary, unique ID:
 *
 * ```
 * /**
 *  * Line 1
 *  #custom_id
 *  * /
 *
 * { custom_id : { text : ' Line 1', links : [] } }
 * ```
 */

/*`` @test */
suite.test('ids', async function () {
    var source = (function () {
        /** #id Line 1 */
        `/*hel*/lo`
    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        id : { text : 'Line 1'}
    });
});
/*``*/

/**
 * And any node can be linked to any other node.
 *
 * ```
 * /** #node_a * /
 *
 * /**
 *  #node_b
 *  @node_a
 *  * /
 *
 * {
 *   node_a : { text : '', links : ['node_b'] },
 *   node_b : { text : '', links : ['node_a'] }
 * }
 * ```
 *
 * If you link to a non-existent node, it will be created for you.
 *
 * ```
 * /**
 *  * My custom method
 *  *
 *  #method_name
 *  @deprecated
 *  * /
 *
 * {
 *     method_name : { text : ' My custom method', links : ['deprecated']},
 *     deprecated : { text : '', links : ['method_name'] }
 * }
 * ```
 *
 * Because nodes are indexed, you can access your links in ``O(1)`` time.
 * This makes it easy do things like filter out deprecated methods
 * or provide a global list of todos, or group modules together.
 */

/*`` @test */
suite.test('linking to non-existent nodes', async function () {
    var source = (function () {
        /**
         * Line 1
         @link
         */

    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : { links : ['link'] },
        link : { links : ['_0'] }
    });
});
/*``*/

/*`` @test */
suite.test('changing ids on a linked node', async function () {
    var source = (function () {
        /**
         @link
         #id
         */

    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        id : { links : ['link'] },
        link : { links : ['id'] }
    });
});
/*``*/

/*`` @test */
suite.test('ids and linking: doing something needless complicated', async function () {
    var source = (function () {
        /**
         #one
         @two
         */

        /** #three */

        /**
         *
         @three
         @four
         #two
         */
    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        one : { links : ['two'] },
        two : { links : ['one', 'three', 'four'] },
        three : { links : ['two'] },
        four : { links : ['two'] }
    });
});
/*``*/

/*`` @test */
suite.test('', true).test('code block', async function () {

    var source = [
        'function () {',
        '    /*' + '``*/',
        '    var x = 5;',
        '    /*' + '``*/',
        '}'
    ].join('\n');

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : { text : 'var x = 5;' }
    });
});
/*``*/


/*`` @test */
suite.test('appending', async function () {
    var source = (function () {
        /** line 1 */
        /*+ line 2 */
    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : { text : ['line 1', 'line 2'].join('\n') }
    });
});
/*``*/

/*`` @test */
suite.test('previous/next', async function () {
    var source = (function () {
        /** line 1 */
        /** #id line 2 */
        /** line 3 */
    }).toString();

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : { previous : null, next : 'id' },
        id : { previous : '_0', next : '_1' },
        _1 : { previous : 'id', next : null }
    });
})
/*``*/

