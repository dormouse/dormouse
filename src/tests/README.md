

# The Testing Suite

Dormouse is tested using [Distilled](https://distilledjs.com).

While Dormouse does not track code coverage, pull requests will still be rejected if they do not include relevant tests.
Tests are expected to test behaviors, not implementation.

When writing new tests, ask yourself if your test could be used in place of an API or documentation.
As a matter of fact, many of Dormouse's tests *are* used as its API!

## Adding Tests

The included harness will automatically run any javascript files that end with the word "Test".
Simply add a new file to a suitable directory.

There are a variety of helper utilities to make writing tests easier.
They are located under the ``node_modules`` folder.
Documentation is compiled under each directory.

Because helpers are placed inside ``node_modules``, you don't need to use relative paths to require them.
For example, use ``require('helpers/assert');`` rather than ``require('../helpers/assert.js');``.