var suite = require('helpers/suite');
var simplify = require('helpers/simplify');
var assert = require('helpers/assert');
var dormouse = require('dormouse');

/**
 * # Properties (.)
 *
 * Properties allow you to add additional metadata to your nodes.
 @markdown
 */
suite = suite.test('Line Prefix: Properties [.]');
suite.test('Basic prefix', async function () {

    var source = (function () {

        /*`` @markdown .source_compiled */
        return [
            `/**`,
            ` .prop1 value 1`,
            ` .prop2 value 2`,
            ` */`
        ].join('\n');
        /*``*/

    }());

    var json = await dormouse(source);
    simplify(json);

    /**
     * A Line prefixed with a dot is converted into a property.
     * - The property name will be set to the string following the line prefix.
     * - The property value will be set to whatever text follows the first space.
     */
    var expected = (function () {
        /*`` @markdown .source_compiled*/
        return {
            properties : {
                prop1 : 'value 1',
                prop2 : 'value 2'
            }
        };
        /*``*/
    }());

    assert.subset(json._0, expected);
});

/** @markdown If no value is provided, the value will be auto-set to ``true``. */
suite.test('Default values', async function () {
    var source = (function () {
        /*`` @markdown .source_compiled */
        return '/** .prop */';
        /*``*/
    }());

    var json = await dormouse(source);
    simplify(json);

    var expected = (function () {
        /*`` @markdown .source_compiled */
        return {
            properties : { prop : true }
        };
        /*``*/
    }());

    /** @markdown You can take advantage of this by doing string checks for unset properties. */
    assert.subset(json._0, expected);
});
