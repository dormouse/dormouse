

# Properties (.)

Properties allow you to add additional metadata to your nodes.

```js
/**
 .prop1 value 1
 .prop2 value 2
 */
```

```js
{
   "properties": {
      "prop1": "value 1",
      "prop2": "value 2"
   }
}
```

If no value is provided, the value will be auto-set to ``true``.

```js
/** .prop */
```

```js
{
   "properties": {
      "prop": true
   }
}
```

You can take advantage of this by doing string checks for unset properties.

# Text (*)

Text lines are the most basic line type.

```js
/**
 *Line 1
 *Line 2
 */
```

Lines prefixed with an asterisk are converted into literal lines of text.

```text 
Line 1
Line 2
```

A common pattern when writing comment blocks is to add a single space at the start of each line.

```js
/**
 * Line 1
 *  Line 2
*/
```

If Dormouse included this space in generated documentation, it could interfere with Markdown formatting.
So up to (and no more than) one space will be removed from the front of each text line.

```text 
Line 1
 Line 2
```

Right-side spacing on the other hand is sometimes required in Markdown.

```js
/**
 * Line 1  
 */
```

So Dormouse includes all right-side whitespace in text lines.

```text 
Line 1  
```

Occasionally, you may find that you want to ignore all whitespace,
or you may simply find that you don't want to type an extra asterisk.

Unprefixed lines are also treated like text lines,
but their whitespace is fully trimmed.

```js
/** Line 1 */
```

```text 
Line 1
```

This behavior can be used anywhere,
but is ideal for quick, one-line comments.