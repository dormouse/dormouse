var suite = require('helpers/suite');
var simplify = require('helpers/simplify');
var assert = require('helpers/assert');
var dormouse = require('dormouse');


/**
 * # Text (*)
 *
 * Text lines are the most basic line type.
 @markdown
 */
suite = suite.test('Line Prefix: Text [*]');
suite.test('Basic prefix', async function () {

    var source = (function () {

        /*``
         .source_compiled
         @markdown
         */
        return [
            `/**`,
            ` *Line 1`,
            ` *Line 2`,
            ` */`
        ].join('\n');
        /*``*/

    }());

    var json = await dormouse(source);
    simplify(json);

    /** @markdown Lines prefixed with an asterisk are converted into literal lines of text. */
    var expected = (function () {
        /*`` @markdown .source_compiled text */
        return ['Line 1', 'Line 2'].join('\n');
        /*``*/
    }());

    assert.strictEqual(json._0.text, expected);
});

/** @markdown A common pattern when writing comment blocks is to add a single space at the start of each line. */
suite.test('Single-space truncation', async function () {
    var source = (function () {

        /*``
         .source_compiled
         @markdown
         */
        return [
            `/**`,
            ` * Line 1`,
            ` *  Line 2`,
            `*/`
        ].join('\n');
        /*``*/

    }());

    var json = await dormouse(source);
    simplify(json);

    /**
     * If Dormouse included this space in generated documentation, it could interfere with Markdown formatting.
     * So up to (and no more than) one space will be removed from the front of each text line.
     @markdown
     */
    var expected = (function () {
        /*`` @markdown .source_compiled text */
        return ['Line 1', ' Line 2'].join('\n');
        /*``*/
    }());

});

/** @markdown Right-side spacing on the other hand is sometimes required in Markdown. */
suite.test('No right-side truncation', async function () {
    var source = (function () {

        /*``
         .source_compiled
         @markdown
         */
        return [
            `/**`,
            ` * Line 1  `,
            ` */`
        ].join('\n');
        /*``*/

    }());

    var json = await dormouse(source);
    simplify(json);

    /** @markdown So Dormouse includes all right-side whitespace in text lines. */
    var expected = (function () {
        /*`` @markdown .source_compiled text */
        return ['Line 1  '].join('\n');
        /*``*/
    }());

    assert.strictEqual(json._0.text, expected);
});

/**
 * Occasionally, you may find that you want to ignore all whitespace,
 * or you may simply find that you don't want to type an extra asterisk.
 *
 * Unprefixed lines are also treated like text lines,
 * but their whitespace is fully trimmed.
 @markdown
 */
suite.test('Unprefixed text lines', async function () {
    var source = (function () {

        /*``
         .source_compiled
         @markdown
         */
        return [
            `/** Line 1 */`
        ].join('\n');
        /*``*/

    }());

    var json = await dormouse(source);
    simplify(json);

    var expected = (function () {
        /*`` @markdown .source_compiled text */
        return ['Line 1'].join('\n');
        /*``*/
    }());

    /**
     * This behavior can be used anywhere,
     * but is ideal for quick, one-line comments.
     @markdown
     */
    assert.strictEqual(json._0.text, expected);
});
