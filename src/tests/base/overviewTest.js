var suite = require('helpers/suite');
var simplify = require('helpers/simplify');
var assert = require('helpers/assert');

/**
 * # Dormouse
 * An unopinionated documentation generator.
 *
 * *Dormouse is in a pre-alpha state.*
 * *You should expect that the API will change, possibly dramatically, before the 1.0 release.*
 *
 @markdown
 */
var dormouse = require('dormouse');

/**
 * ## What is it?
 *
 * Dormouse reads block comments and then converts them into JSON.
 @markdown
 */
suite.test('Basic functionality', async function () {

    var source = (function () {

        /*``
         .source_compiled
         .dormouse
         @markdown
         */
        return [
            `/**`,
            ` * Takes a number and returns its double.`,
            ` #foo`,
            ` */`,
            `function foo (x) {`,
            ``,
            `    /** @foo The foo function only passes on Tuesdays. */`,
            `    if (now.getDay() !== "Tuesday") {`,
            `        throw new Error("It's not Tuesday");`,
            `    }`,
            ``,
            `    return x * 2;`,
            `}`
        ].join('\n');
        /*``*/

    }());

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        foo : { text : 'Takes a number and returns its double.', links : ['_0'] },
        _0 : { text : 'The foo function only passes on Tuesdays.', links : ['foo'] }
    });
});

/**
 * ## JSON?
 *
 * Yep! Whereas most documentation generators output HTML,
 * Dormouse outputs your documentation as a JSON graph.
 * This gives you a large amount of control over how your documentation can be used.
 * It also allows you to pipe your documentation into other programs or processors.
 @markdown
 */

/**
 * graph Docs {
 *   class -- Foo
 *
 *   Foo -- foo
 *   Foo -- bar
 *
 *   method -- foo
 *   method -- bar
 *
 *   deprecated -- bar
 * }
 *
 @markdown
 .dot example-hierarchy
 */

/**
 * ## Why Dormouse?
 *
 * Most documentation generators are Frameworks.
 * They only allow you to describe your code using a pre-defined list of tags and behaviors.
 *
 * Dormouse rejects that entire concept. You can use any ID you want for a node,
 * and any node can be linked to any other node.
 * You can even append custom properties to nodes to further describe them.
 *
 * Dormouse's sense of freedom allows you to adopt your documentation to fit your code, not the other way around.
 @markdown
 */

/** @markdown * ## Basic Overview */
suite.test('IDs', async function () {

    /**
     * **Custom IDs:**
     *
     * IDs are optional for nodes.
     * Whenever you supply one, your node will be indexed by that ID.
     * This means that you can access any node you've given an ID in O(1) time.
     @markdown
     */
    var source = (function () {

        /*``
         .source_compiled
         .dormouse
         @markdown
         */
        return '/** #foo An arbitrary block of text */';
        /*``*/
    }());

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        foo : { text : 'An arbitrary block of text' }
    });
});

suite.test('Linking', async function () {

    /**
     * **Linking:**
     *
     * Any node can be linked to any other node.
     * If you link to a node that doesn't exist yet, it will be created for you.
     *
     * Notice that below, tags without IDs are indexed by their creation timestamp.
     * This reduces the chance of collisions if you want to merge multiple graphs of documentation.
     @markdown
     */
    var source = (function () {

        /*``
         .source_compiled
         .dormouse
         @markdown
         */
        return [
            '/**',
            ' * My custom method',
            ' @deprecated',
            ' */'
        ].join('\n');
        /*``*/
    }());

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : { links : ['deprecated'] }
    });
});

/**
 * **Properties:**
 *
 * Nodes can also be given custom properties.
 * These should be used to provide additional metadata about the node.
 @markdown
 */
suite.test('Properties', async function () {
    var source = (function () {

        /*``
         .source_compiled
         .dormouse
         @markdown
         */
        return [
            '/**',
            ' * My custom method',
            ' .created August 28, 2017',
            ' */'
        ].join('\n');
        /*``*/
    }());

    var json = await dormouse(source);
    simplify(json);

    assert.subset(json, {
        _0 : { properties : { created : 'August 28, 2017' } }
    });
});

/**
 * ----
 * *[View this file's source](src/tests/base/overviewTest.js)*
 @markdown
 */
