

# Dormouse
An unopinionated documentation generator.

*Dormouse is in a pre-alpha state.*
*You should expect that the API will change, possibly dramatically, before the 1.0 release.*


## What is it?

Dormouse reads block comments and then converts them into JSON.

```js
/**
 * Takes a number and returns its double.
 #foo
 */
function foo (x) {

    /** @foo The foo function only passes on Tuesdays. */
    if (now.getDay() !== "Tuesday") {
        throw new Error("It's not Tuesday");
    }

    return x * 2;
}
```
```js
{
   "_1505652070564": {
      "id": "_1505652070564",
      "links": [
         "foo"
      ],
      "properties": {},
      "text": "The foo function only passes on Tuesdays.",
      "location": 133,
      "previous": "foo",
      "next": null
   },
   "foo": {
      "id": "foo",
      "links": [
         "_1505652070564"
      ],
      "properties": {},
      "text": "Takes a number and returns its double.",
      "location": 55,
      "previous": null,
      "next": "_1505652070564"
   }
}
```

## JSON?

Yep! Whereas most documentation generators output HTML,
Dormouse outputs your documentation as a JSON graph.
This gives you a large amount of control over how your documentation can be used.
It also allows you to pipe your documentation into other programs or processors.


<img src="./.images/example-hierarchy.png"  alt="graph Docs {
  class -- Foo
  Foo -- foo
  Foo -- bar
  method -- foo
  method -- bar
  deprecated -- bar
}"/>

## Why Dormouse?

Most documentation generators are Frameworks.
They only allow you to describe your code using a pre-defined list of tags and behaviors.

Dormouse rejects that entire concept. You can use any ID you want for a node,
and any node can be linked to any other node.
You can even append custom properties to nodes to further describe them.

Dormouse's sense of freedom allows you to adopt your documentation to fit your code, not the other way around.

## Basic Overview 

**Custom IDs:**

IDs are optional for nodes.
Whenever you supply one, your node will be indexed by that ID.
This means that you can access any node you've given an ID in O(1) time.

```js
/** #foo An arbitrary block of text */
```
```js
{
   "foo": {
      "id": "foo",
      "links": [],
      "properties": {},
      "text": "An arbitrary block of text",
      "location": 38,
      "previous": null,
      "next": null
   }
}
```

**Linking:**

Any node can be linked to any other node.
If you link to a node that doesn't exist yet, it will be created for you.

Notice that below, tags without IDs are indexed by their creation timestamp.
This reduces the chance of collisions if you want to merge multiple graphs of documentation.

```js
/**
 * My custom method
 @deprecated
 */
```
```js
{
   "_1506097199281": {
      "id": "_1506097199281",
      "links": [
         "deprecated"
      ],
      "properties": {},
      "text": "My custom method",
      "location": 40,
      "previous": null,
      "next": null
   },
   "deprecated": {
      "id": "deprecated",
      "links": [
         "_1506097199281"
      ],
      "properties": {},
      "text": ""
   }
}
```

**Properties:**

Nodes can also be given custom properties.
These should be used to provide additional metadata about the node.

```js
/**
 * My custom method
 .created August 28, 2017
 */
```
```js
{
   "_1506097385110": {
      "id": "_1506097385110",
      "links": [],
      "properties": {
         "created": "August 28, 2017"
      },
      "text": "My custom method",
      "location": 53,
      "previous": null,
      "next": null
   }
}
```

----
*[View this file's source](src/tests/base/overviewTest.js)*